package com.kenchlightyear;
import org.openqa.selenium.By;		
import org.openqa.selenium.WebDriver;		
import org.openqa.selenium.chrome.ChromeDriver;		

public class MyClass {				
    		
    public static void main(String[] args) {									
        String baseUrl = "https://www.kenchlightyear.com/";
        System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver");					
        WebDriver driver = new ChromeDriver();					
        		
        driver.get(baseUrl);					
        //click on the "Facebook" logo on the upper left portion		
			driver.findElement(By.xpath("//div[@id=\"fh5co-logo\"]")).click();					

			//verify that we are now back on Facebook's homepage		
			if (driver.getTitle().equals("KenchLightyear — Affordable Enterprise Support")) {							
            System.out.println("We are back at KenchLightyear's homepage");					
        } else {			
            System.out.println("We are NOT in KenchLightyear's homepage");					
        }		
				driver.close();		

    }		
}
