# selenium
Testing Demo using Selenium

## Setup
`export CLASSPATH=.:client-combined-3.141.59.jar:libs/commons-exec-1.3.jar:libs/guava-25.0-jre.jar:libs/okhttp-3.11.0.jar:libs/okio-1.14.0.jar`

## Compile
`javac com/kenchlightyear/MyClass.java`

## Run
`joebert@RSCPH01-LT004:~/selenium$ java com.kenchlightyear.MyClass`
